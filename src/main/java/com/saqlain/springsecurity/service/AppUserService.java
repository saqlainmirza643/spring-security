package com.saqlain.springsecurity.service;

import com.saqlain.springsecurity.dto.RegisterRequest;
import com.saqlain.springsecurity.entity.AppUser;

public interface AppUserService {
    boolean checkUsernameExists(String username);
    AppUser registerUser(RegisterRequest request);
}
