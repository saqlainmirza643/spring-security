package com.saqlain.springsecurity.service.impl;

import com.saqlain.springsecurity.dto.RegisterRequest;
import com.saqlain.springsecurity.entity.AppUser;
import com.saqlain.springsecurity.entity.Authority;
import com.saqlain.springsecurity.repo.AppUserRepo;
import com.saqlain.springsecurity.repo.AuthorityRepo;
import com.saqlain.springsecurity.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AppUserServiceImpl implements AppUserService {

    private final AppUserRepo userRepo;
    private final AuthorityRepo authorityRepo;
    private final PasswordEncoder passwordEncoder;

    public boolean checkUsernameExists(String username) {
        return userRepo.findByUsername(username).isPresent();
    }

    public AppUser registerUser(RegisterRequest user) {
        AppUser newUser = new AppUser();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Authority> authorities = new HashSet<>();
        for (String authorityName : user.getAuthorities()) {
            Authority authority = authorityRepo.findByName(authorityName);
            if (authority != null) {
                authorities.add(authority);
            }
        }
        newUser.setAuthorities(authorities);
        return userRepo.save(newUser);
    }
}
