package com.saqlain.springsecurity.controller;

import com.saqlain.springsecurity.dto.RegisterRequest;
import com.saqlain.springsecurity.entity.AppUser;
import com.saqlain.springsecurity.service.AppUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class AppUserController {
    private final AppUserService appUserService;

    @GetMapping("/test")
    public String getMessage() {
        return "hello";
    }

    @GetMapping("/user/test")
    public String getPublicMessage() {
        return "public message";
    }

    @GetMapping("/public/expired")
    public String getExpiredMessage() {
        return "page is expired";
    }

    @PostMapping("/public/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
        boolean isExist = appUserService.checkUsernameExists(request.getUsername());
        if (isExist) {
            return ResponseEntity.badRequest().body("Username already exists");
        }
        AppUser registeredUser = appUserService.registerUser(request);
        return ResponseEntity.ok().body(registeredUser);
    }

}
