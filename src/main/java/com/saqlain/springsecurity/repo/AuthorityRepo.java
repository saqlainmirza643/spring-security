package com.saqlain.springsecurity.repo;

import com.saqlain.springsecurity.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepo extends JpaRepository<Authority, Integer> {
    Authority findByName(String authorityName);
}
